#!/usr/bin/env python3

# Noah Spierings - HBO Intern KPMG NL
# This program parses the data within Cobalt Strike logs, reading/analyzing/scanning and converting it into usable (visualized) information
# Currently, the following features exist:
# - compromised systems information within modifiable .json file
# - compromised systems information parsed within a .xlsx file
# - number of tokens that have been used visualized
# - lateral movement visual in HTML file
# - network communication visual in HTML file
# - file/file structure modifications/uploads analyzer displayed within the terminal
# Version 2.3 - 20-04-2021

# --- importing the necessary libraries --- #
import subprocess
import sys
import os
import time
import re
import socket
import ipaddress
import webbrowser
import csv
import json

missing = []

try:
    import pandas as pd
except ImportError as e:
    missing.append(str(e.name))
    pd = None
try:
    import plotly.express as px
except ImportError as e:
    missing.append(str(e.name))
    px = None
try:
    from pandasql import sqldf
except ImportError as e:
    missing.append(str(e.name))
    sqldf = None
try:
    from datetime import datetime
except ImportError as e:
    missing.append(str(e.name))
    datetime = None
try:
    import xlsxwriter
except ImportError as e:
    missing.append(e.name)
    xlsxwriter = None
try:
    import sqlite3 as sql
except ImportError as e:
    missing.append(e.name)
    sql = None
try:
    from urllib.request import pathname2url
except ImportError as e:
    missing.append(e.name)
    pathname2url = None

if len(missing) > 0:
    print("Some libraries are missing:")
    for lib in missing:
        print(f"        {lib}")

    answer = ''
    while answer != 'y' and answer != 'n':
        if len(missing) > 1:
            answer = str(input("\nWould you like to download these libraries? (y/n): "))
        else:
            answer = str(input("\nWould you like to download this library? (y/n): "))

    if answer == 'n':
        exit()
    elif answer == 'y':
        try:
            for lib in missing:
                subprocess.check_call([sys.executable, "-m", "pip", "install", lib])
        except Exception as error:
            print(f"Error occurred while tyring to download library: {error}")
            exit()
# ----------------------------------------- #


class CobaltStrikeVisualizer:
    def __init__(self):
        # options #
        self.verbose = False
        self.help = False
        self.blacklist = False
        self.cobalt_strike_information_input = False
        self.lateral_movement = False
        self.scan_modification = False
        self.communication_visual = False
        self.compromised_information_xlsx_file = False

        # file related options #
        self.cobalt_log_dir = ''
        self.cobalt_log_file = ''
        self.beacon_information_file = ''
        self.beacon_information_output_file = 'Compromised_systems_information.json'
        self.lateral_movement_visual_filename = 'Lateral_movement_visual.html'
        self.communication_visual_filename = 'Communication_visual.html'
        self.compromised_information_xlsx_filename = 'Compromised_systems_information'

    def arguments(self):
        arguments_list = ['-h', '--help', '-v', '--verbose', '-p', '--path', '-i', '--input', '-o', '--output', '-b', '--blacklist',
                          '-l', '--lateral', '-m', '--modification', '-c', '--communication', '-x', '--xlsx']
        commandline_arguments = sys.argv[1:]

        for argument in commandline_arguments:
            if argument not in arguments_list and ('-' in argument or '--' in argument):
                print(f"\nUnable to recognize argument {argument}. Type -h or --help for commandline arguments.")
                return False

        if len(commandline_arguments) == 0:
            print("""\n
                                       ____ ____ ___  ____ _    ___    ____ ___ ____ _ _  _ ____    _  _ _ ____ _  _ ____ _    _ ___  ____ ____ 
                                       |    |  | |__] |__| |     |     [__   |  |__/ | |_/  |___    |  | | [__  |  | |__| |    |   /  |___ |__/ 
                                       |___ |__| |__] |  | |___  |     ___]  |  |  \ | | \_ |___     \/  | ___] |__| |  | |___ |  /__ |___ |  \               
                                                                                                         
                                                                                                   
                                                                Made by: Noah Spierings - HBO Intern KPMG NL
                        
                        Cobalt Strike Visualizer is an program created for the Red Team at KPMG. It parses Cobalt Strile .log files by analyzing/scanning the
                        generated data and converting it into usable (visualized) information for Red Teaming reports. Currelty, this program supports the
                        following features:
                        
                         - compromised systems information within modifiable .json file
                         - compromised systems information parsed within a .xlsx file
                         - number of tokens that have been used visualized
                         - lateral movement visual in HTML file
                         - network communication visual in HTML file
                         - file/file structure modifications/uploads analyzer displayed within the terminal
                         - semi automatic depended library downloader (asks for permission)
                        
                        Version: 2.3
                        Last updated: 20-04-2021
            
                        Optional arguments:

                            -h, --help          shows help message and exits the program
                            
                            -v, --verbose       enable verbose option, this will display more text than usual
                            
                            -p, --path          input the Cobalt Strike log path (e.g. /CobaltStrike/logs, /./logs, logs)
                                                -d <directory path>, --directory <directory path>
                                                
                            -o, --output        input the name of the compromised systems information output file
                                                the standard filename has been configured into beacon_information.json
                                                -o <filename>, --output <filename>
                                                
                            -i, --input         input the name of the compromised systems information input file.
                                                The program will use this data while it is running, instead of the default generated data.
                                                Useful for modifications in data and showing the correct visuals/information
                                                -i <filename>, --input <filename>
                                                
                            -b, --blacklist     use the current configured blacklist. The program will scan the existing 
                                                hostnames within the blacklist and will use it as a filter 
                                                
                            -l, --lateral       create a HTML page visualizing lateral movement from the Red teaming engagement.
                                                The filename can be changed by adding it after the argument, otherwise default will be chosen
                                                -l <filename>, --lateral <filename>
                            
                            -m, --modification  receive printed information on what (file) modifications are still on the client's system.
                                                This includes information on upload, mkdir, cp, mv with rm filter. Cannot me used with -i/--input
                                                
                            -c, --communication create a visualization of the link & unlink command communication of the Red teaming engagement.
                                                The filename can be changed by adding it after the argument, otherwise default will be chosen
                                                -c <filename>, --communication <filename>
                                                
                            -x, --xlsx          parse the generated (or inputted) compromised system information into a excel file.
                                                You will also receive a visual on the amount of used tokens
                                                -x <filename>, --xlsx <filename>
                                                 
                                                
                        Examples:

                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -o information_beacons -v
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -i information_beacons -v -c 
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -c -l -x -m -b -v
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -c <filename> -l <filename> -x -m -b -v 
                        """)
            return False

        if '-h' in commandline_arguments or '--help' in commandline_arguments:
            print("""\n
                        Optional arguments:

                            -h, --help          shows help message and exits the program
                            
                            -v, --verbose       enable verbose option, this will display more text than usual
                            
                            -p, --path          input the Cobalt Strike log path (e.g. /CobaltStrike/logs, /./logs, logs)
                                                -d <directory path>, --directory <directory path>
                                                
                            -o, --output        input the name of the compromised systems information output file
                                                the standard filename has been configured into beacon_information.json
                                                -o <filename>, --output <filename>
                                                
                            -i, --input         input the name of the compromised systems information input file.
                                                The program will use this data while it is running, instead of the default generated data.
                                                Useful for modifications in data and showing the correct visuals/information
                                                -i <filename>, --input <filename>
                                                
                            -b, --blacklist     use the current configured blacklist. The program will scan the existing 
                                                hostnames within the blacklist and will use it as a filter 
                                                
                            -l, --lateral       create a HTML page visualizing lateral movement from the Red teaming engagement.
                                                The filename can be changed by adding it after the argument, otherwise default will be chosen
                                                -l <filename>, --lateral <filename>
                            
                            -m, --modification  receive printed information on what (file) modifications are still on the client's system.
                                                This includes information on upload, mkdir, cp, mv with rm filter. Cannot me used with -i/--input
                                                
                            -c, --communication create a visualization of the link & unlink command communication of the Red teaming engagement.
                                                The filename can be changed by adding it after the argument, otherwise default will be chosen
                                                -c <filename>, --communication <filename>
                                                 
                            -x, --xlsx          parse the generated (or inputted) compromised system information into a excel file.
                                                You will also receive a visual on the amount of used tokens
                                                -x <filename>, --xlsx <filename>
                                                 
                                                
                        Examples:

                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -o information_beacons -v
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -i information_beacons -v -c
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -c -l -x -m -b -v
                            Cobalt_stike_visualizer.py -p c:/users/user/documents/logs_cobalt -c <filename> -l <filename> -x -m -b -v 
                        """)
            return False

        if ('-i' in commandline_arguments or '--input' in commandline_arguments) and ('-o' in commandline_arguments or '--output' in commandline_arguments):
            print('Error occurred: cannot create information file and use information file at the same time (-i/--input and -o/--output)')
            return False

        if '-v' in commandline_arguments or '--verbose' in commandline_arguments:
            self.verbose = True

        if '-l' in commandline_arguments or '--lateral' in commandline_arguments:
            self.lateral_movement = True
            for pos, argument in enumerate(commandline_arguments):
                if '-l' == argument or '--lateral' == argument:
                    if len(commandline_arguments) > (pos + 1):
                        if commandline_arguments[pos + 1] not in arguments_list:
                            if '.html' not in commandline_arguments[pos + 1]:
                                commandline_arguments[pos + 1] += '.html'
                                self.lateral_movement_visual_filename = commandline_arguments[pos + 1]

        if '-c' in commandline_arguments or '--communication' in commandline_arguments:
            self.communication_visual = True
            for pos, argument in enumerate(commandline_arguments):
                if '-c' == argument or '--communication' == argument:
                    if len(commandline_arguments) > (pos + 1):
                        if commandline_arguments[pos + 1] not in arguments_list:
                            if '.html' not in commandline_arguments[pos + 1]:
                                commandline_arguments[pos + 1] += '.html'
                                self.lateral_movement_visual_filename = commandline_arguments[pos + 1]

        if '-x' in commandline_arguments or '--xlsx' in commandline_arguments:
            self.compromised_information_xlsx_file = True
            for pos, argument in enumerate(commandline_arguments):
                if '-x' == argument or '--xlsx' == argument:
                    if len(commandline_arguments) > (pos + 1):
                        if commandline_arguments[pos + 1] not in arguments_list:
                            if '.xlsx' not in commandline_arguments[pos + 1]:
                                commandline_arguments[pos + 1] += '.xlsx'
                                self.compromised_information_xlsx_filename = commandline_arguments[pos + 1]

        if '-m' in commandline_arguments or '--modification' in commandline_arguments:
            self.scan_modification = True

        if '-b' in commandline_arguments or '--blacklist' in commandline_arguments:
            self.blacklist = True

        if '-p' in commandline_arguments or '--path' in commandline_arguments:
            for pos, argument in enumerate(commandline_arguments):
                if '-p' == argument or '--path' == argument:
                    self.cobalt_log_dir = os.path.abspath(commandline_arguments[pos + 1])

        if '-o' or '--output' in commandline_arguments:
            for pos, argument in enumerate(commandline_arguments):
                if '-o' == argument or '--output' == argument:
                    if '.json' not in commandline_arguments[pos + 1]:
                        commandline_arguments[pos + 1] += '.json'
                    self.beacon_information_output_file = commandline_arguments[pos + 1]

        if '-i' or '--input' in commandline_arguments:
            for pos, argument in enumerate(commandline_arguments):
                if '-i' == argument or '--input' == argument:
                    if '.json' not in commandline_arguments[pos + 1]:
                        commandline_arguments[pos + 1] += '.json'
                    self.cobalt_strike_information_input = True
                    self.beacon_information_file = os.path.abspath(commandline_arguments[pos + 1])

                    while os.path.exists(self.beacon_information_file) is False:
                        print(f"\nOops, an error occurred: file {self.beacon_information_file} does not exist.")
                        self.beacon_information_file = os.path.abspath(str(input("Please, try again: ")))
                        if '.json' not in self.beacon_information_file:
                            self.beacon_information_file += '.json'
        return True

    def main(self):
        start_time = time.time()
        if self.arguments() is True:
            compromised_systems, dic_hosts, json_beacon_lateral_movement, hosts_upload, smb_connections = self.ParsingCobaltData().main_parsing_cobalt_data()
            json_beacon_information, json_beacon_lateral_movement = self.ReadingCobaltData().main_reading_cobalt_data(compromised_systems, dic_hosts,
                                                                                                                      json_beacon_lateral_movement,
                                                                                                                      hosts_upload)
            self.VisualizingCobaltData().main_visualizing_cobalt_data(json_beacon_lateral_movement, json_beacon_information, smb_connections)

            print(f"\n--- {time.time() - start_time} seconds ---")

    class ParsingCobaltData:

        def __init__(self):
            if CobaltStrikeVisualizer.verbose is True:
                print("\nLoaded the following settings")
                if CobaltStrikeVisualizer.blacklist is True:
                    print(f"Blacklist enabled: {CobaltStrikeVisualizer.blacklist}")
                print(f"Scanning logs in following path: {CobaltStrikeVisualizer.cobalt_log_dir}")
                print(f"Filename beacon information: {CobaltStrikeVisualizer.beacon_information_output_file}")
                print(f"Making lateral movement visual: {CobaltStrikeVisualizer.lateral_movement}")
                if CobaltStrikeVisualizer.lateral_movement is True:
                    print(f"    Saved under filename: {CobaltStrikeVisualizer.lateral_movement_visual_filename}")
                print(f"Scanning uploaded files: {CobaltStrikeVisualizer.scan_modification}")
                print(f"Making communication visual: {CobaltStrikeVisualizer.communication_visual}")
                if CobaltStrikeVisualizer.communication_visual is True:
                    print(f"    Saved under filename: {CobaltStrikeVisualizer.communication_visual_filename}")
                if CobaltStrikeVisualizer.compromised_information_xlsx_file is True:
                    print(f"Saving information into .xlsx file: {CobaltStrikeVisualizer.compromised_information_xlsx_file}")
                    print(f"    Saved under filename: {CobaltStrikeVisualizer.compromised_information_xlsx_filename}")
                    print(f"Making a graph visual on token usage: {CobaltStrikeVisualizer.compromised_information_xlsx_file}")
                print(f"Verbose mode on: {CobaltStrikeVisualizer.verbose}\n")

            print("\nScanning for Cobalt Strike beacons")

            # file environment vars #
            self.current_log_path = ''
            self.current_file_year = ''
            self.current_IP = ''

            # scanning lateral movement vars #
            self.hosts = []
            self.smb_connections = {}
            self.hosts_upload = {}
            self.dic_hosts = {}
            self.compromised_systems = []
            self.compromised_systems_beacons = []
            self.lateral_movement = []
            self.json_beacon_lateral_movement = []
            self.list_systems = []
            if CobaltStrikeVisualizer.blacklist is True:
                self.blacklist = self.read_cobalt_hostname_blacklist()

        def read_cobalt_hostname_blacklist(self):
            try:
                with open("Cobalt_strike_blacklist.csv", newline="") as file:
                    reader = csv.reader(file, delimiter=",")
                    next(reader)
                    blacklist = [row for row in reader]

                file.close()
                if CobaltStrikeVisualizer.verbose is True:
                    print("\nLoaded hostname blacklist:")
                    for hostname in blacklist:
                        print(f"    {hostname[0]:>}")
                    print("\n")
                return blacklist
            except Exception as error:
                print(f"Oops, an error has occurred while trying to open file: Cobalt_strike_blacklist.csv: \n{error}")

        # search (within the given path) all the Cobalt Strike beacon_..logs. Once found, trigger cobalt_identify_beacon to collect the beacon's [metadata] #
        def cobalt_log_finder(self, file_name, dir_name):
            file_path = os.path.join(dir_name, file_name)
            # if directory basename is == to Cobalt Strike date format #
            if os.path.basename(file_path).isnumeric():
                self.current_file_year = '20' + os.path.basename(file_path)[:2]

            # if directory is an IP address like Cobalt Strike date format #
            if os.path.isdir(file_path):
                if '.' in os.path.basename(file_path):
                    if socket.inet_aton(os.path.basename(file_path)):
                        self.current_IP = os.path.basename(file_path)

                for files in os.listdir(file_path):
                    self.cobalt_log_finder(files, file_path)

            elif os.path.isfile(file_path):
                # only find the beacon_... Cobalt format files #
                if 'beacon_' in os.path.basename(file_path):
                    self.current_log_path = file_path
                    self.cobalt_identify_beacon(file_path)

        # from the cobalt_log_finder, collect part of the beacon's [metadata] and create a (not complete) list for the lateral movement structure #
        # to complete the lateral movement list, all the host ([metadata]) have to be identified with their corresponding beacon_..log file #
        # (e.g. beacon_986314331.log -> 10.0.2.15 has to be converted into 192.168.1.18 -> 10.0.2.15) #
        # the collection filled with [metadata] will be used to pinpoint the plausible cause (e.g. techniques & tokens used to compromise the machine) #
        def cobalt_identify_beacon(self, file_path):

            # open the Beacon log file #
            with open(file_path, 'r', encoding='utf-8') as beacon_file:

                first_line = beacon_file.readline()

                if re.search('\[metadata]', first_line):
                    # finding the amount of hops for each beacon #
                    right_system, left_system = re.search(" (\S*\S);", first_line).group(1), re.search("\[metadata] (\S*\S) ", first_line).group(1)
                    time_beacon = str(
                        datetime.strptime(self.current_file_year + "/" + re.search("../.. ..:..:.. UTC", first_line).group()[:-4], "%Y/%m/%d %H:%M:%S"))
                    hostname = re.search('computer: (\S*\S);', first_line).group(1)
                    compromised_system = self.current_IP + ', ' + re.search('computer: (\S*\S);', first_line).group(
                        1) + ', ' + re.search('user: (.*); process', first_line).group(1) + ', ' + re.search('process: (\S*\S);', first_line).group(1)
                    hostname_upload = self.current_IP + ', ' + re.search('computer: (\S*\S);', first_line).group(1)

                    if CobaltStrikeVisualizer.blacklist is True:
                        # loading blacklist #
                        blacklist = self.blacklist

                        # filtering with blacklist #
                        for host in blacklist:
                            if hostname == host[0]:
                                return False

                    self.compromised_systems.append(compromised_system)
                    self.compromised_systems_beacons.append({str(datetime.strptime(
                        self.current_file_year + "/" + re.search('../.. ..:..:.. UTC', first_line).group()[:-4],
                        '%Y/%m/%d %H:%M:%S')): compromised_system})

                    if 'beacon_' not in left_system and 'beacon_' not in right_system and left_system != 'unknown' and right_system != 'unknown':
                        if ipaddress.ip_address(left_system) not in ipaddress.ip_network(
                                '192.168.0.0/16') and ipaddress.ip_address(
                            left_system) not in ipaddress.ip_network('172.16.0.0/12') and ipaddress.ip_address(left_system) not in ipaddress.ip_network(
                            '10.0.0.0/8'):
                            left_system = "C2 server"

                    if 'left_system' and 'right_system' in locals():
                        self.hosts.append(
                            {'system': right_system + ', ' + hostname, 'time': time_beacon, 'hops': 0, 'connection': left_system, 'hostname': hostname})

                    if os.path.basename(file_path) not in list(self.dic_hosts.keys()):
                        self.dic_hosts[os.path.basename(file_path)] = compromised_system + ', ' + re.search('pid: (.*);', first_line).group(1)

                    if CobaltStrikeVisualizer.verbose is True:
                        print(f"Found beacon {compromised_system}, initialized on: {time_beacon}")
            beacon_file.close()

        # sort the collection of [metadata] #
        # this is needed to properly utilize the dates to scan the logs #
        def sort_list_beacons(self):
            print(f"\nTotal number of beacons: {len(self.compromised_systems_beacons)}")
            print(f"Sorting beacons by date ...")
            dates = []
            for time_beacon in self.compromised_systems_beacons:
                keys_list = list(time_beacon)
                key = keys_list[0]
                dates.append(key)
            dates.sort(key=lambda date: datetime.strptime(date, '%Y-%m-%d %H:%M:%S'))

            list_beacon_sorted = []
            for date in dates:
                for item in self.compromised_systems_beacons:
                    if item.get(date):
                        list_beacon_sorted.append(item)
            self.compromised_systems_beacons = list_beacon_sorted

        # the lateral movements with beacon_..log in the [metadata] will be be replaced by their corresponding IP address #
        # this function also sorts the lateral movement list #
        # it is essential for receiving the movements from machine to machine, not C2 server to machine, as those aren't first #
        def filling_lateral_movement_list(self):
            print("Filling lateral movement path information ...")

            list_hosts = []
            self.hosts.sort(key=lambda x: x['time'])

            for item in self.hosts:
                right_system, left_system = item['system'], item['connection']

                if right_system != 'C2 server':
                    if 'beacon_' in right_system:
                        for information in self.dic_hosts:
                            if right_system in information:
                                item['system'] = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                    if 'beacon_' in left_system:
                        for information in self.dic_hosts:
                            if left_system in information:
                                item['connection'] = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                    if ', ' not in right_system:
                        for information in self.dic_hosts:
                            if left_system in information:
                                item['connection'] = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                    if ', ' not in left_system:
                        for information in self.dic_hosts:
                            if left_system in information:
                                item['connection'] = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]

            for pos, item in enumerate(self.hosts):
                dic_beacons = {pos: item['time']}
                for pos2, beacon in enumerate(self.hosts):
                    if item['system'] == beacon['system']:
                        dic_beacons[pos2] = beacon['time']
                if len(dic_beacons) != 1:
                    min_val = min(dic_beacons.values())
                    remaining = dic_beacons.keys() - (k for k, v in dic_beacons.items() if v != min_val)
                    for item2 in self.hosts:
                        if item2['time'] == dic_beacons[list(remaining)[0]] and item2 not in list_hosts:
                            list_hosts.append(item2)
                else:
                    for item2 in self.hosts:
                        remaining = dic_beacons.keys()
                        if item2['time'] == dic_beacons[list(remaining)[0]] and item2 not in list_hosts:
                            list_hosts.append(item2)
            self.hosts = list_hosts
            self.hosts.insert(0, {'system': 'C2 server', 'time': '', 'hops': -1, 'connection': 'None', 'hostname': 'C2 server'})

            for item in self.hosts:
                if item['system'] != "C2 server":
                    for beacon in self.hosts:
                        if item['connection'] == beacon['system']:
                            item['hops'] = beacon['hops'] + 1
                            break
            print("Saving lateral movement path information ...")
            for item in self.hosts:
                self.save_information_hops(system=item['system'], time=item['time'], hops=item['hops'],
                                           connection=item['connection'], hostname=item['hostname'])

        # saving the lateral movement json structure #
        # this structure is used to make the lateral movement visual #
        def save_information_hops(self, system, hostname, time, hops, connection):
            json_structure_hops = {system:
                {
                    'hostname': hostname,
                    'time': time,
                    'hops': hops,
                    'connected': connection
                }
            }

            self.json_beacon_lateral_movement.append(json_structure_hops)

        def file_modification_scanner(self, file_name, dir_name):
            file_path = os.path.join(dir_name, file_name)
            # if directory basename is == to Cobalt Strike date format #
            if os.path.basename(file_path).isnumeric():
                self.current_file_year = '20' + os.path.basename(file_path)[:2]

            # if directory is an IP address like Cobalt Strike date format #
            if os.path.isdir(file_path):
                if '.' in os.path.basename(file_path):
                    if socket.inet_aton(os.path.basename(file_path)):
                        self.current_IP = os.path.basename(file_path)

                for files in os.listdir(file_path):
                    self.file_modification_scanner(files, file_path)

            elif os.path.isfile(file_path):
                # only find the beacon_... Cobalt format files #
                if 'beacon_' in os.path.basename(file_path):
                    self.current_log_path = file_path

                    # open the Beacon log file #
                    with open(file_path, 'r', encoding='utf-8') as beacon_file:

                        first_line = beacon_file.readline()

                        if re.search('\[metadata]', first_line):
                            hostname = re.search('computer: (\S*\S);', first_line).group(1)
                            hostname_upload = self.current_IP + ', ' + re.search('computer: (\S*\S);', first_line).group(1)
                        elif os.path.basename(self.current_log_path) in list(self.dic_hosts.keys()):
                            for information in self.dic_hosts:
                                if os.path.basename(self.current_log_path) in information:
                                    hostname = self.dic_hosts[information].split(', ')[1]
                                    hostname_upload = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                        else:
                            return False

                        if CobaltStrikeVisualizer.blacklist is True:
                            # loading blacklist #
                            blacklist = self.blacklist

                            # filtering with blacklist #
                            for host in blacklist:
                                if hostname == host[0]:
                                    return False

                        if hostname_upload not in self.hosts_upload.keys():
                            self.hosts_upload[hostname_upload] = {'upload': [], 'mkdir': [], 'cp': [], 'mv': [], 'rm': []}
                        with open(file_path, 'r', encoding='utf-8') as beacon_file2:
                            upload_command_error_catching = None

                            # make the code a bit more optimal #
                            for line in beacon_file2:
                                if re.search('\[input]', line) and (
                                        re.search('upload (.*)', line) or re.search('mkdir (.*)', line) or re.search('cp (.*)', line) or re.search('mv (.*)',
                                                                                                                                                   line)):
                                    if re.search('\\\\(.*)', line) and re.search('C\$', line):
                                        hostname_upload = re.search('\\\\(.*)\\$', line).group(1).replace('\\', '')[:-1]
                                        for information in self.dic_hosts:
                                            if hostname_upload.lower() in self.dic_hosts[information].lower():
                                                hostname_upload = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                                        if hostname_upload not in self.hosts_upload.keys():
                                            self.hosts_upload[hostname_upload] = {'upload': [], 'mkdir': [], 'cp': [], 'mv': [], 'rm': []}

                                    if re.search('upload (.*)', line):
                                        self.hosts_upload[hostname_upload]['upload'].append(re.search('upload (.*)', line).group(1))
                                        upload_command_error_catching = True
                                    elif re.search('mkdir (.*)', line):
                                        self.hosts_upload[hostname_upload]['mkdir'].append(re.search('mkdir (.*)', line).group(1))
                                    elif re.search('cp (.*)', line):
                                        self.hosts_upload[hostname_upload]['cp'].append(re.search('cp (.*)', line).group(1))
                                    elif re.search('mv (.*)', line):
                                        self.hosts_upload[hostname_upload]['mv'].append(re.search('mv (.*)', line).group(1))

                                elif re.search('\[input]', line) and re.search('rm (.*)', line):
                                    if re.search('\\\\(.*)', line) and re.search('C\$', line):
                                        hostname_upload = re.search('\\\\(.*)\\$', line).group(1).replace('\\', '')[:-1]
                                        for information in self.dic_hosts:
                                            if hostname_upload.lower() in self.dic_hosts[information].lower():
                                                hostname_upload = self.dic_hosts[information].split(', ')[0] + ', ' + self.dic_hosts[information].split(', ')[1]
                                        if hostname_upload not in self.hosts_upload.keys():
                                            self.hosts_upload[hostname_upload] = {'upload': [], 'mkdir': [], 'cp': [], 'mv': [], 'rm': []}
                                    self.hosts_upload[hostname_upload]['rm'].append(re.search('rm (.*)', line).group(1))
                                elif re.search('\[error] could not upload file:', line) and upload_command_error_catching is True:
                                    del self.hosts_upload[hostname_upload]['upload'][-1]
                                    upload_command_error_catching = False
                            beacon_file2.close()

        def link_and_unlink_communication_scanner(self, file_name, dir_name):
            file_path = os.path.join(dir_name, file_name)
            # if directory basename is == to Cobalt Strike date format #
            if os.path.basename(file_path).isnumeric():
                self.current_file_year = '20' + os.path.basename(file_path)[:2]

            # if directory is an IP address like Cobalt Strike date format #
            if os.path.isdir(file_path):
                if '.' in os.path.basename(file_path):
                    if socket.inet_aton(os.path.basename(file_path)):
                        self.current_IP = os.path.basename(file_path)

                for files in os.listdir(file_path):
                    self.link_and_unlink_communication_scanner(files, file_path)

            elif os.path.isfile(file_path):
                # only find the beacon_... Cobalt format files #
                if 'beacon_' in os.path.basename(file_path):
                    self.current_log_path = file_path

                    # open the Beacon log file #
                    with open(file_path, 'r', encoding='utf-8') as beacon_file:

                        first_line = beacon_file.readline()

                        if re.search('\[metadata]', first_line):
                            hostname = re.search('computer: (\S*\S);', first_line).group(1)
                            system = self.current_IP + ', ' + re.search('computer: (\S*\S);', first_line).group(
                                1) + ', ' + re.search('user: (.*); process', first_line).group(1) + ', ' + re.search('process: (\S*\S);', first_line).group(
                                1) + ', ' + re.search('pid: (.*);', first_line).group(1)

                        elif os.path.basename(self.current_log_path) in list(self.dic_hosts.keys()):
                            for information in self.dic_hosts:
                                if os.path.basename(self.current_log_path) in information:
                                    hostname = self.dic_hosts[information].split(', ')[1]
                                    system = self.dic_hosts[information]
                        else:
                            return False

                        if CobaltStrikeVisualizer.blacklist is True:
                            # loading blacklist #
                            blacklist = self.blacklist

                            # filtering with blacklist #
                            for host in blacklist:
                                if hostname.lower() == host[0].lower():
                                    return False
                        beacon_file.close()

                    link_smb_named_pipe = ''
                    child_system = ''

                    # open the Beacon log file #
                    with open(file_path, 'r', encoding='utf-8') as beacon_file:

                        for line in beacon_file:
                            # IMPLEMENT DATA FOR COMMUNICATION VISUAL #
                            if re.search('> link (.*)', line):
                                child_system = re.search('> link (\S*\S)', line).group(1)
                                if re.search('> link (.*) (.*)', line):
                                    link_smb_named_pipe = re.search('> link (.*) (.*)', line).group(2)

                            if re.search('> unlink (.*)', line):
                                child_system = re.search('> unlink (\S*\S)', line).group(1)
                                if re.search('> unlink (.*) (.*)', line):
                                    unlink_smb_beacon_pid = re.search('> unlink (.*) (.*)', line).group(2)
                                    for information in self.dic_hosts:
                                        if (child_system and unlink_smb_beacon_pid) in self.dic_hosts[information]:
                                            child_system = self.dic_hosts[information]

                            if re.search('established link to child beacon: (.*)', line):
                                if system.split(', ')[0] not in self.smb_connections.keys():
                                    self.smb_connections[system] = {'named_pipe': link_smb_named_pipe, 'child': child_system}
                                elif system.split(', ')[0] in self.smb_connections.keys():
                                    for parent in list(self.smb_connections.keys()):
                                        if system.split(', ')[0] == parent:
                                            self.smb_connections[system] = self.smb_connections.pop(parent)

                            if re.search('established link to parent beacon: (.*)', line):
                                if re.search('established link to parent beacon: (.*)', line).group(1) in self.smb_connections.keys():
                                    for parent in self.smb_connections.keys():
                                        if re.search('established link to parent beacon: (.*)', line).group(1) in parent:
                                            self.smb_connections[parent]['child'] = system
                                else:
                                    self.smb_connections[re.search('established link to parent beacon: (.*)', line).group(1)] = {'named_pipe': '',
                                                                                                                                 'child': system}
                    beacon_file.close()

        def main_parsing_cobalt_data(self):
            self.cobalt_log_finder(CobaltStrikeVisualizer.cobalt_log_dir, CobaltStrikeVisualizer.cobalt_log_file)
            self.sort_list_beacons()
            self.filling_lateral_movement_list()
            if CobaltStrikeVisualizer.scan_modification is True:
                print("Scanning systems for file modifications ...")
                self.file_modification_scanner(CobaltStrikeVisualizer.cobalt_log_dir, CobaltStrikeVisualizer.cobalt_log_file)
            if CobaltStrikeVisualizer.communication_visual is True:
                print("Scanning logs for communication visual ...")
                self.link_and_unlink_communication_scanner(CobaltStrikeVisualizer.cobalt_log_dir, CobaltStrikeVisualizer.cobalt_log_file)
            return self.compromised_systems_beacons, self.dic_hosts, self.json_beacon_lateral_movement, self.hosts_upload, self.smb_connections

    class ReadingCobaltData:

        def __init__(self):
            # file environment vars #
            self.current_file_year = ''
            self.current_IP = ''

            # scanning lateral movement vars #
            self.lateral_commands = self.read_cobalt_lateral_commands()
            self.token_commands = self.read_cobalt_token_commands()
            self.analyze_log_path = ''
            self.current_log_path = ''
            self.compromised_systems = []
            self.compromised_systems_beacons = []
            self.dic_hosts = {}
            self.beacon1 = ''
            self.beacon2 = ''
            self.current_target = ''
            self.json_beacon_lateral_movement = []
            self.json_beacon_information = []
            if CobaltStrikeVisualizer.cobalt_strike_information_input is True:
                with open(f"{CobaltStrikeVisualizer.beacon_information_file}", "r") as json_file:
                    self.json_beacon_information = json.load(json_file)
            if CobaltStrikeVisualizer.blacklist is True:
                with open("Cobalt_strike_blacklist.csv", newline="") as file:
                    reader = csv.reader(file, delimiter=",")
                    next(reader)
                    self.blacklist = [row for row in reader]
                file.close()

        # initializing the set lateral_movement technique commands #
        # the program will use this information to scan for possible injection techniques #
        # (e.g. SharpWPI.exe, Cobalt Strike jump and remote-exec commands) #
        def read_cobalt_lateral_commands(self):
            try:
                with open("Cobalt_strike_attack_commands.csv", newline="") as file:
                    reader = csv.reader(file, delimiter=",")
                    next(reader)
                    list_commands = [row for row in reader]
                file.close()
                if CobaltStrikeVisualizer.verbose is True:
                    print("\nLoaded lateral movement commands:")
                    for method in list_commands:
                        print(f"    {method[0]:>}: {method[1]:>}, {method[2]:>}, {method[3]:>}, {method[4]:>}")
                return list_commands
            except Exception as error:
                print(f"Oops, an error has occurred while trying to open file: Cobalt_strike_attack_commands.csv: \n{error}")

        # initializing the set lateral_movement token commands #
        # the program will use this information to scan for possible token techniques #
        # (e.g. make_token, steal_token, pth) #
        def read_cobalt_token_commands(self):
            try:
                with open("Cobalt_strike_token_commands.csv", newline="") as file:
                    reader = csv.reader(file, delimiter=",")
                    next(reader)
                    list_commands = [row for row in reader]
                    extract_token_successful = {}
                    for method in list_commands:
                        extract_token_successful[method[0]] = method[4]

                file.close()
                if CobaltStrikeVisualizer.verbose is True:
                    print("\nLoaded lateral movement commands:")
                    for method in list_commands:
                        print(f"    {method[0]:>}: {method[1]:>}, {method[2]:>}, {method[3]:>}, {method[4]:>}")
                return list_commands, extract_token_successful
            except Exception as error:
                print(f"Oops, an error has occurred while trying to open file: Cobalt_strike_token_commands.csv: \n{error}")

        # to find the possible causes of the beacon's initialization, information has to be analyzed between the beacons #
        # this function selects the directories that correspond with the selected beacon's date #
        # this does not only improve the performance, but can be used to actively check if the plausible command has been preformed before the beacons existence #
        # it will then trigger cobalt_log_finder, to scan the logs within the directory #
        # with of without possible causes, information will be saved with the save_lateral_movement function #
        def process_collection_beacons(self, file, directory):
            if CobaltStrikeVisualizer.verbose is True:
                print("\nAnalyzing logs for the following beacons:")
            else:
                print("Analyzing the logs ...")

            for item in self.compromised_systems_beacons:
                self.beacon2, self.beacon1 = self.beacon1, item
                date = list(item)[0]
                self.current_target, date_folder = list(item.values())[0], date[2:10].replace('-', '')

                if date_folder == file:
                    self.locate_beacon_logs(file, directory)
                elif date_folder != file:
                    self.locate_beacon_logs(date_folder, os.path.join(directory, file))

                targets_list = []
                target = self.current_target + ', ' + str(datetime.strptime(list(self.beacon1)[0], '%Y-%m-%d %H:%M:%S'))
                for key in self.json_beacon_information:
                    targets_list.append(list((key.keys()))[0])
                if target not in targets_list:
                    self.save_lateral_movement(target, hosts='None', token_value='None', token_time='None',
                                               token_command='None',
                                               token_path_log='None', token_line_number='None', lateral_value='None',
                                               lateral_time='None', lateral_command='None', lateral_path_log='None',
                                               lateral_line_number='None', lateral_score_commands='None',
                                               lateral_alternative_value=[], lateral_alternative_time=[],
                                               lateral_alternative_command=[], lateral_alternative_path_log=[],
                                               lateral_alternative_line_number=[], lateral_alternative_score_commands=[],
                                               alternative_hosts=[], token_alternative_value=[],
                                               token_alternative_line_number=[], token_alternative_time=[],
                                               token_alternative_path_log=[], token_alternative_command=[])

        # locate_beacon_logs will locate all the beacon_..log files within the specified directory #
        # the input comes from the function process_collection_beacons (e.g. 200915, 201126) #
        # once a log has been located, this function will trigger function; analyzing_logs_between beacons #
        def locate_beacon_logs(self, file_name, dir_name):
            file_path = os.path.join(dir_name, file_name)
            # if directory basename is == to Cobalt Strike date format #
            if os.path.basename(file_path).isnumeric():
                self.current_file_year = '20' + os.path.basename(file_path)[:2]

            # if directory is an IP address like Cobalt Strike date format #
            if os.path.isdir(file_path):
                if '.' in os.path.basename(file_path):
                    if socket.inet_aton(os.path.basename(file_path)):
                        self.current_IP = os.path.basename(file_path)

                for files in os.listdir(file_path):
                    self.locate_beacon_logs(files, file_path)

            elif os.path.isfile(file_path):
                # only find the beacon_... Cobalt format files #
                if 'beacon_' in os.path.basename(file_path):
                    # save the path of the current file #
                    self.current_log_path = file_path
                    # read the current Beacon log file #
                    self.analyzing_logs_between_beacons(file_path)

        # finding possible causes for the beacons existence comes from this function #
        # It gets it's information from two different configuration files that state the token & lateral techniques that have been used #
        # at the end, information will be saved into a json file with the function save_lateral_movement #
        def analyzing_logs_between_beacons(self, file_path):
            # state the variables used for information
            hosts, alternative_hosts = 'None', []
            token_value, token_alternative_value = 'None', []
            token_time, token_alternative_time = 'None', []
            token_command, token_alternative_command = 'None', []
            token_path_log, token_alternative_path_log = 'None', []
            token_line_number, token_alternative_line_number = 'None', []
            lateral_value, lateral_alternative_value = 'None', []
            lateral_time, lateral_alternative_time = 'None', []
            lateral_command, lateral_alternative_command = 'None', []
            lateral_score_commands, lateral_alternative_score_commands = 'None', []
            lateral_path_log, lateral_alternative_path_log = 'None', []
            lateral_line_number, lateral_alternative_line_number = 'None', []
            list_systems = []
            lateral_commands = self.lateral_commands
            token_commands, extract_token_successful = self.token_commands
            # ----------------------------------------

            # after the init check, we want to check if it was due to a lateral movement technique #
            # go through all the information and check if it fits with the one of the targets #
            target = self.current_target + ', ' + str(datetime.strptime(list(self.beacon1)[0], '%Y-%m-%d %H:%M:%S'))
            line_counter = 1
            prop_counter_lateral = 0
            try:
                with open(self.current_log_path, 'r', encoding='utf8') as file:
                    first_line = file.readline()
                    # gather beacon's host #
                    if re.search('\[metadata]', first_line):
                        beacon_id = re.search('pid:......', first_line).group().replace(';', '')[4:]
                        beaconid = int(re.sub('[a-z]', '', beacon_id))
                        host = self.current_IP + ', ' + re.search('computer: (\S*\S);', first_line).group(
                            1) + ', ' + re.search('user: (.*); process', first_line).group(1) + ', ' + re.search('process: (\S*\S);', first_line).group(1)
                        hostname = re.search('computer: (\S*\S);', first_line).group(1)
                    elif os.path.basename(self.current_log_path) in list(self.dic_hosts.keys()):
                        host = self.dic_hosts[os.path.basename(self.current_log_path)]
                        hostname = self.dic_hosts[os.path.basename(self.current_log_path)].split(', ')[1]
                    else:
                        host = 'Unknown'
                        hostname = 'Unknown'

                    if CobaltStrikeVisualizer.blacklist is True:
                        blacklist = self.blacklist
                        for host_blacklist in blacklist:
                            if hostname.lower() == host_blacklist[0].lower():
                                return False

                    for line in file:
                        line_counter += 1

                        # search for the input commando's #
                        if re.search('\[input]', line):
                            command = line[line.find('>') + 2:-1]
                            command_time = re.search('../.. ..:..:.. UTC', line).group()[:-4]
                            date = self.current_file_year
                            timestamp_command = datetime.strptime(date + '/' + command_time,
                                                                  '%Y/%m/%d %H:%M:%S')

                            # scalable token techniques #
                            if timestamp_command <= datetime.strptime(list(self.beacon1)[0], "%Y-%m-%d %H:%M:%S"):
                                for method in token_commands:
                                    if re.search(method[1], command):
                                        if method[2] != 'None' and re.search(method[2], command):
                                            token_value_var, token_time_var = re.search(method[2], command).group(1).replace(
                                                '"', ''), str(timestamp_command)
                                            token_path_log_var, token_line_number_var = self.current_log_path, str(line_counter)
                                            token_command_var = method[0]
                                        else:
                                            token_value_var, token_time_var = '', str(timestamp_command)
                                            token_path_log_var, token_line_number_var = self.current_log_path, str(line_counter)
                                            token_command_var = method[0]
                                    elif re.search('rev2self', command):
                                        if host != 'Unknown':
                                            token_value_var = ''.join([host.split(', ')[1], '\\', host.split(', ')[2]])
                                        else:
                                            token_value_var = 'Unknown'
                                        token_time_var, token_path_log_var = str(timestamp_command), self.current_log_path
                                        token_line_number_var, token_command_var = str(line_counter), 'rev2self'

                            # checking if commands time falls between the two beacons.
                            # If this implementation component does not exist, program will try commands before last beacon.
                            check_command = False
                            if self.beacon2 != "":
                                if datetime.strptime(list(self.beacon2)[0],
                                                     '%Y-%m-%d %H:%M:%S') <= timestamp_command <= datetime.strptime(list(self.beacon1)[0], '%Y-%m-%d %H:%M:%S'):
                                    check_command = True
                            elif timestamp_command <= datetime.strptime(list(self.beacon1)[0], '%Y-%m-%d %H:%M:%S'):
                                check_command = True

                            if check_command is True:
                                # scalable lateral movement techniques #
                                for method in lateral_commands:
                                    if re.search(method[1], command):
                                        if method[2] != 'None' and re.search(method[2], command):
                                            token_value_var, token_time_var = re.search(method[2], command).group(1).replace(
                                                '"', ''), str(timestamp_command)
                                            token_path_log_var, token_line_number_var = self.current_log_path, str(line_counter)
                                            token_command_var = method[0]
                                        lateral_alternative_value.append(method[0])
                                        lateral_alternative_time.append(str(timestamp_command))
                                        lateral_alternative_path_log.append(self.current_log_path)
                                        lateral_alternative_line_number.append(str(line_counter))
                                        lateral_alternative_command.append(command)

                                        if 'token_value_var' in locals():
                                            token_alternative_value.append(token_value_var)
                                            token_alternative_time.append(token_time_var)
                                            token_alternative_command.append(token_command_var)
                                            token_alternative_path_log.append(token_path_log_var)
                                            token_alternative_line_number.append(token_line_number_var)
                                        else:
                                            if host != 'Unknown':
                                                token_alternative_value.append(
                                                    ' '.join([host.split(', ')[1] + '\\' + host.split(', ')[2]]))
                                            else:
                                                token_alternative_value.append('Unknown')
                                            token_alternative_time.append('None')
                                            token_alternative_command.append('None')
                                            token_alternative_path_log.append('None')
                                            token_alternative_line_number.append('None')

                                        alternative_hosts.append(host)
                                        score_text = "Low"
                                        prop_counter_lateral = 0

                        # giving a probability of the lateral movement command #
                        if len(lateral_alternative_score_commands) != len(lateral_alternative_value):
                            if prop_counter_lateral == 0:
                                lateral_alternative_score_commands.append('Low')

                        # checking if token command was executed successfully #
                        for method in token_commands:
                            if method[3] != 'None' and re.search(method[3], line):
                                if 'token_command_var' in locals() and token_command_var != 'rev2self':
                                    if extract_token_successful[token_command_var]:
                                        if len(token_alternative_value) != 0:
                                            if token_alternative_value[-1] == '':
                                                token_alternative_value[-1] = re.search(method[3], line).group(1)
                                            else:
                                                token_value_var = re.search(method[3], line).group(1)
                                        else:
                                            token_value_var = re.search(method[3], line).group(1)
                                break

                        # checking if lateral command was executed successfully #
                        for method in lateral_commands:
                            if method[3] != 'None' and re.search(method[3], line) and len(
                                    lateral_alternative_score_commands) != 0:
                                lateral_alternative_score_commands[-1] = 'Medium'

                    # checking if command was for target and changing to high prop#
                    for pos, command in enumerate(lateral_alternative_command):
                        for method in lateral_commands:
                            if method[4] != 'None' and re.search(method[4], command):
                                for info in target.split(', '):
                                    if re.search(info, re.search(method[4], command).group(1)):
                                        if lateral_alternative_score_commands[pos] == 'Low':
                                            lateral_alternative_score_commands[pos] = 'Medium'
                                        elif lateral_alternative_score_commands[pos] == 'Medium':
                                            lateral_alternative_score_commands[pos] = 'High'

                    # implement relative from command w/ multiple commands #
                    for i in range(len(lateral_alternative_score_commands)):
                        if len(lateral_alternative_score_commands) != 0 and lateral_alternative_score_commands[-i] == 'High':
                            lateral_alternative_score_commands[-1] = 'Very high'
                            break
                        elif len(lateral_alternative_score_commands) != 0 and lateral_alternative_score_commands[-i] == 'Medium':
                            lateral_alternative_score_commands[-1] = 'High'
                        elif len(lateral_alternative_score_commands) != 0 and lateral_alternative_score_commands[-i] == 'Low':
                            lateral_alternative_score_commands[-1] = 'Medium'

                    # parse information into json structured dic
                    if len(lateral_alternative_value) != 0:
                        if len(token_alternative_value) == 0:
                            token_alternative_value.append('None')
                            token_alternative_time.append('None')
                            token_alternative_path_log.append('None')
                            token_alternative_line_number.append('None')
                            token_alternative_command.append('None')

                        for pos, information in enumerate(lateral_alternative_value):
                            if lateral_alternative_score_commands[pos] == 'Very high':
                                lateral_value, lateral_time = lateral_alternative_value.pop(pos), lateral_alternative_time.pop(
                                    pos)
                                lateral_score_commands, lateral_command = lateral_alternative_score_commands.pop(
                                    pos), lateral_alternative_command.pop(pos)
                                lateral_path_log, lateral_line_number = lateral_alternative_path_log.pop(
                                    pos), lateral_alternative_line_number.pop(pos)
                                hosts, token_value = alternative_hosts.pop(pos), token_alternative_value.pop(pos)
                                token_time, token_command = token_alternative_time.pop(pos), token_alternative_command.pop(pos)
                                token_path_log, token_line_number = token_alternative_path_log.pop(
                                    pos), token_alternative_line_number.pop(pos)
                                if CobaltStrikeVisualizer.verbose is True:
                                    print("        | Found techniques with Very high score |")

                        self.save_lateral_movement(target, hosts, token_value, token_time, token_command, token_path_log,
                                                   token_line_number, lateral_value,
                                                   lateral_time, lateral_command, lateral_path_log, lateral_line_number,
                                                   lateral_score_commands, lateral_alternative_value, lateral_alternative_time,
                                                   lateral_alternative_command, lateral_alternative_path_log,
                                                   lateral_alternative_line_number, lateral_alternative_score_commands,
                                                   alternative_hosts,
                                                   token_alternative_value, token_alternative_time, token_alternative_command,
                                                   token_alternative_path_log, token_alternative_line_number)

                file.close()

            except Exception as error:
                print(f"Oops, an error has occurred while trying to analyze log {self.current_log_path}: \n{error}")

        # save_lateral_movement saves the information gathered from analyzing_logs_between_beacons #
        # the format can be seen below #
        def save_lateral_movement(self, target, hosts, token_value, token_time, token_command, token_path_log,
                                  token_line_number, lateral_value,
                                  lateral_time, lateral_command, lateral_path_log, lateral_line_number,
                                  lateral_score_commands, lateral_alternative_value, lateral_alternative_time,
                                  lateral_alternative_command, lateral_alternative_path_log,
                                  lateral_alternative_line_number, lateral_alternative_score_commands, alternative_hosts,
                                  token_alternative_value, token_alternative_time, token_alternative_command,
                                  token_alternative_path_log, token_alternative_line_number):

            json_structure = {target:
                {"token":
                    {
                        "value": token_value,
                        "time": token_time,
                        "command": token_command,
                        "log_path": token_path_log,
                        "line_number": token_line_number
                    },
                    "lateral":
                        {
                            "value": lateral_value,
                            "time": lateral_time,
                            "command": lateral_command,
                            "log_path": lateral_path_log,
                            "line_number": lateral_line_number,
                            "score": lateral_score_commands,
                            "host": hosts,
                        },
                    "token_alternative":
                        {
                            "value": token_alternative_value,
                            "time": token_alternative_time,
                            "command": token_alternative_command,
                            "log_path": token_alternative_path_log,
                            "line_number": token_alternative_line_number
                        },
                    "lateral_alternative":
                        {
                            "value": lateral_alternative_value,
                            "time": lateral_alternative_time,
                            "command": lateral_alternative_command,
                            "log_path": lateral_alternative_path_log,
                            "line_number": lateral_alternative_line_number,
                            "score": lateral_alternative_score_commands,
                            "host": alternative_hosts,
                        }
                }
            }
            self.json_beacon_information.append(json_structure)
            if CobaltStrikeVisualizer.verbose is True:
                print(f"    {self.current_target + ', ' + str(datetime.strptime(list(self.beacon1)[0], '%Y-%m-%d %H:%M:%S'))}")

        def save_json_data(self, file_name, data):
            try:
                with open(file_name, 'w', encoding='utf-8') as json_file:
                    json.dump(data, json_file, ensure_ascii=False, indent=4)
                    json_file.close()
                print("\nInformation on compromised systems saved!")
            except Exception as error:
                print(f"Oops, an error has occurred while trying to save {data}: \n{error}")

        def modifications_files_filter(self, hosts_upload):
            print('\nHere are the modifications made that have yet to be removed')
            print(
                "\nHost                                 Command   File                                                                                       Location")
            for host in hosts_upload:
                for removed in hosts_upload[host]['rm']:
                    for file in hosts_upload[host]['upload']:
                        if os.path.basename(removed) in file:
                            hosts_upload[host]['upload'].remove(file)
                    for directory in hosts_upload[host]['mkdir']:
                        if os.path.basename(removed) in directory:
                            hosts_upload[host]['mkdir'].remove(directory)
                    for copy in hosts_upload[host]['cp']:
                        if os.path.basename(removed) in copy:
                            hosts_upload[host]['cp'].remove(copy)
                    for move in hosts_upload[host]['mv']:
                        if os.path.basename(removed) in move:
                            hosts_upload[host]['mv'].remove(move)

                if len(hosts_upload[host]['upload']) != 0:
                    for file in hosts_upload[host]['upload']:
                        if '(' not in file and ')' not in file:
                            file_print = file
                            location = file
                        else:
                            file_print = file[:file.find(' (')]
                            location = file[file.find(' (') + 1:].replace('(', '').replace(')', '')
                        print("{:<36} upload    {:<90} {:<40}".format(host, file_print, location))
                if len(hosts_upload[host]['mkdir']) != 0:
                    for file in hosts_upload[host]['mkdir']:
                        print("{:<36} mkdir     {:<90} {:<40}".format(host, file, file))
                if len(hosts_upload[host]['cp']) != 0:
                    for file in hosts_upload[host]['cp']:
                        if '" ' in file:
                            print("{:<36} cp        {:<90} {:<40}".format(host, file.split('" ')[0].replace('"', ''), file.split('" ')[1]).replace('"', ''))
                        elif ' "' in file:
                            print("{:<36} cp        {:<90} {:<40}".format(host, file.split(' "')[0].replace('"', ''), file.split(' "')[1]).replace('"', ''))
                        else:
                            print("{:<36} cp        {:<90} {:<40}".format(host, file.split(' ')[0], file.split(' ')[1]))
                if len(hosts_upload[host]['mv']) != 0:
                    for file in hosts_upload[host]['mv']:
                        if '" ' in file:
                            print("{:<36} mv        {:<90} {:<40}".format(host, file.split('" ')[0].replace('"', ''), file.split('" ')[1]).replace('"', ''))
                        elif ' "' in file:
                            print("{:<36} cp        {:<90} {:<40}".format(host, file.split(' "')[0].replace('"', ''), file.split(' "')[1]).replace('"', ''))
                        else:
                            print("{:<36} mv        {:<90} {:<40}".format(host, file.split(' ')[0], file.split(' ')[1]))

        def main_reading_cobalt_data(self, compromised_systems, dic_hosts, json_beacon_lateral_movement, hosts_upload):
            self.compromised_systems_beacons, self.dic_hosts, self.json_beacon_lateral_movement = compromised_systems, dic_hosts, json_beacon_lateral_movement
            if CobaltStrikeVisualizer.cobalt_strike_information_input is False:
                self.process_collection_beacons(CobaltStrikeVisualizer.cobalt_log_file, CobaltStrikeVisualizer.cobalt_log_dir)
                self.save_json_data(CobaltStrikeVisualizer.beacon_information_output_file, self.json_beacon_information)
            if CobaltStrikeVisualizer.scan_modification is True:
                self.modifications_files_filter(hosts_upload)
            return self.json_beacon_information, self.json_beacon_lateral_movement

    class VisualizingCobaltData:

        def __init__(self):
            pass

        def main_visualizing_cobalt_data(self, json_beacon_lateral_movement, lateral_movement_data, smb_connection):
            if CobaltStrikeVisualizer.lateral_movement is True:
                self.lateral_movement_visual(json_beacon_lateral_movement, lateral_movement_data)
            if CobaltStrikeVisualizer.communication_visual is True:
                self.communication_visual(smb_connection, json_beacon_lateral_movement)
            if CobaltStrikeVisualizer.compromised_information_xlsx_file is True:
                self.excel_information_cobalt_data(lateral_movement_data)

        # converting the information from the json_lateral_movement into a .xlsx file with an graph showing the tokens and their number of usage #
        def excel_information_cobalt_data(self, lateral_movement_data):
            with open(
                    'C:/Users/nspierings/OneDrive - KPMG/Documents/Security Assessments/Python Lateral Movement/project-redelk-intern/Compromised_systems_information.json',
                    'r') as json_file:
                data = json.load(json_file)

            #data = lateral_movement_data
            # setting in order of columns #
            columns_list = ["date_beacon", "target_ip", "target_computer_name", "target_username",
                            "target_process", "host_ip", "host_computer_name", "host_username", "host_process",
                            "token_value", "token_time", "token_command", "token_log_path",
                            "token_log_line_number",
                            "lateral_value", "lateral_time", "lateral_command", "lateral_log_path", "lateral_log_line_number",
                            "lateral_score"]

            # creating the data structure #
            dataframe_log_data = pd.DataFrame(columns=columns_list)

            # get all the information from the logs #
            # getting all the keys #
            for log_data in data:
                key = list(log_data)[0]

                # setting information for beacon and it's host #
                date_beacon = key.split(', ')[4]
                target_ip = key.split(', ')[0]
                target_computer_name = key.split(', ')[1]
                target_username = key.split(', ')[2]
                target_process = key.split(', ')[3]

                # setting token/token_command information #
                token_time = log_data[key]["token"]["time"]
                token_value = log_data[key]["token"]["value"]
                token_command = log_data[key]["token"]["command"]
                token_log_path = log_data[key]["token"]["log_path"]
                token_log_line_number = log_data[key]["token"]["line_number"]

                # setting lateral movement/lateral movement_command information #
                lateral_value = log_data[key]["lateral"]["value"]
                lateral_time = log_data[key]["lateral"]["time"]
                lateral_command = log_data[key]["lateral"]["command"]
                lateral_score = log_data[key]["lateral"]["score"]
                lateral_log_line_number = log_data[key]["lateral"]["line_number"]
                lateral_log_path = log_data[key]["lateral"]["log_path"]

                # setting host information #
                if log_data[key]["lateral"]["host"] == "None":
                    host_ip = ""
                    host_computer_name = ""
                    host_username = ""
                    host_process = ""
                else:
                    host_ip = log_data[key]["lateral"]["host"].split(', ')[0]
                    host_computer_name = log_data[key]["lateral"]["host"].split(', ')[1]
                    host_username = log_data[key]["lateral"]["host"].split(', ')[2]
                    host_process = log_data[key]["lateral"]["host"].split(', ')[3]

                dic_structure = {'date_beacon': date_beacon, 'target_ip': target_ip, 'target_computer_name': target_computer_name,
                                 'target_username': target_username,
                                 'target_process': target_process, 'token_value': token_value, 'token_time': token_time,
                                 'token_command': token_command, 'token_log_path': token_log_path,
                                 'token_log_line_number': token_log_line_number, 'lateral_value': lateral_value,
                                 'lateral_time': lateral_time, 'lateral_command': lateral_command,
                                 'lateral_log_path': lateral_log_path,
                                 'lateral_log_line_number': lateral_log_line_number, 'lateral_score': lateral_score,
                                 'host_ip': host_ip,
                                 'host_computer_name': host_computer_name, 'host_username': host_username,
                                 'host_process': host_process}

                dataframe_log_data = dataframe_log_data.append(pd.Series(dic_structure), ignore_index=True)  # [columns_list]
                dataframe_log_data = dataframe_log_data.replace(['None'], '')
                dataframe_log_data['date_beacon'] = pd.to_datetime(dataframe_log_data['date_beacon'], format="%Y-%m-%d %H:%M:%S")

            try:
                dburi = 'file:{}?mode=rw'.format(pathname2url('dataframe_log_data.db'))
                connection = sql.connect(dburi, uri=True)
            except sql.OperationalError:
                connection = sql.connect('dataframe_log_data.db')
                dataframe_log_data.to_sql('dataframe_log_data', connection)

            # NOTE: it looks like SQL lite is being used instead of regular SQL #
            q1 = "SELECT target_ip as IP, target_computer_name as Computername, target_username as Username, target_process as Process, date_beacon as Date_Beacon from dataframe_log_data"
            query_compromized_systems = "SELECT date_beacon as 'Date initialization beacon', target_ip as IP, target_computer_name as Computername, target_username as Username, target_process as Process, token_value as 'Used token' from dataframe_log_data ORDER BY IP ASC"
            query_compromized_systems_lateral_movement = "SELECT date_beacon as 'Date initialization beacon', target_ip as IP, target_computer_name as Computername, target_username as Username, target_process as Process, lateral_command as 'Lateral Movement command', token_value as 'Token user' from dataframe_log_data WHERE lateral_command != '' ORDER BY IP ASC"
            query_compromized_users = "SELECT token_time as 'Date created token', token_value as 'Token user', host_process as 'Process on host', lateral_time as 'Date used token' from dataframe_log_data WHERE lateral_time AND token_value != '' ORDER BY lateral_time"
            query_compromized_users_total = "SELECT token_value as 'Token user', COUNT(token_value) as 'Times used' from dataframe_log_data WHERE token_value !='' GROUP BY token_value"

            # setting the writer for writing into the excel file #
            writer = pd.ExcelWriter('Export Cobal Strike.xlsx', engine='xlsxwriter')

            # putting the whole frame into excel file #
            dataframe_log_data.to_excel(writer, sheet_name='Export of Cobalt Logs')

            # using the queries for other dataframes #
            s1 = pd.read_sql(q1, connection)
            dataframe_compromized_systems = pd.read_sql(query_compromized_systems, connection)
            dataframe_compromized_systems_lateral_movement = pd.read_sql(query_compromized_systems_lateral_movement, connection)
            dataframe_query_compromized_users = pd.read_sql(query_compromized_users, connection)
            dataframe_query_compromized_users_total = pd.read_sql(query_compromized_users_total, connection)

            # writing into excel file
            s1.to_excel(writer, sheet_name='Export of compromised systems')
            dataframe_compromized_systems.to_excel(writer, sheet_name='Export of compromised systems')
            dataframe_compromized_systems_lateral_movement.to_excel(writer, startcol=10, sheet_name='Export of compromised systems')
            dataframe_query_compromized_users.to_excel(writer, sheet_name='Export of compromised users')
            dataframe_query_compromized_users_total.to_excel(writer, startcol=7, sheet_name='Export of compromised users')
            writer.save()

            # visuals #
            fig = px.bar(dataframe_query_compromized_users_total, y='Times used', x='Token user')
            fig.show()

        # converting the information from the json_lateral_movement into a tree graph that resembles the injection path #
        def lateral_movement_visual(self, json_beacon_lateral_movement, lateral_movement_data):
            string_nodes = ''
            string_edges = ''
            list_hosts = []
            for information in json_beacon_lateral_movement:
                key = list(information.keys())[0]
                host = key
                technique = ''
                colour = 'rgb(0, 94, 184)'
                hostname = information[key]['hostname']
                connection = information[key]['connected']
                if host not in list_hosts:
                    for beacon in lateral_movement_data:
                        if host in list(beacon)[0] and beacon[list(beacon)[0]]["lateral"]["value"] != 'None':
                            technique = beacon[list(beacon)[0]]["lateral"]["value"]
                            break
                    if host == 'C2 server':
                        string_nodes += '                {id: ' + "'" + host + "'" + ', label: ' + "'" + hostname + "'" + ', image: DIR + "C2_server_icon.png"' + ', shape: "image"' + '},\n'
                    else:
                        if technique != '':
                            with open("Cobalt_strike_attack_commands.csv", newline="") as file:
                                reader = csv.reader(file, delimiter=",")
                                next(reader)
                                list_commands = [row for row in reader]
                            file.close()
                            for method in list_commands:
                                if technique == method[0]:
                                    colour = method[5] + ',' + method[6] + ',' + method[7]
                        # the extra space aligns the lines, check HTML code #
                        if connection == 'C2 server':
                            string_nodes += '                {id: ' + "'" + host + "'" + ', label: ' + "'" + host.split(', ')[
                                1] + "'" + ', image: DIR + "Computer_icon.png"' + ', shape: "image"' + ', color: "' + colour + '"' + '},\n'
                        else:
                            string_nodes += '                {id: ' + "'" + host + "'" + ', label: ' + "'" + host.split(', ')[
                                1] + "'" + ', image: DIR + "Computer_icon.png"' + ', shape: "image"' + ', color: "' + colour + '"' + '},\n'
                        string_edges += '                {from: ' + "'" + host + "'" + ', to: ' + "'" + connection + "'" + '},\n'
                        list_hosts.append(host)

            html_lateral_movement_visual = """
                <html>
                <head>
                    <script type="text/javascript" src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>
    
                    <style type="text/css">
                        #mynetwork {
                            width: 1800px;
                            height: 800px;
                            border: 1px solid rgb(26, 9, 182);
                        }
                    </style>
                </head>
                <body>
                    <div id="mynetwork"><div class="vis-network" tabindex="0" style="position: relative; 
                    overflow: hidden; touch-action: pan-y; user-select: none; -webkit-user-drag: none; 
                    -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 100%;"><canvas 
                    width="1000" height="400" style="position: relative; touch-action: none; user-select: none; 
                    -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 
                    100%;"></canvas></div><div class="vis-configuration-wrapper"><div class="vis-configuration 
                    vis-config-item vis-config-s0"></div><div class="vis-configuration vis-config-item 
                    vis-config-s0"><div class="vis-configuration vis-config-header">Layout changer</div></div><div 
                    class="vis-configuration vis-config-item vis-config-s2"><div class="vis-configuration 
                    vis-config-label vis-config-s2">hierarchical:</div><input type="checkbox" 
                    class="vis-configuration vis-config-checkbox"></div></div></div>
    
                <script type="text/javascript">
                    // create an array with nodes
                    var DIR = "images/network-components/";
    
                    var nodes = new vis.DataSet([
                        """ + string_nodes + """
                    ]);
    
                    // create an array with edges
                    var edges = new vis.DataSet([
                        """ + string_edges + """
                    ]);
    
                    // create a network
                    var container = document.getElementById('mynetwork');
    
                    // provide the data in the vis format
                    var data = {
                        nodes: nodes,
                        edges: edges
                    };
                    var options = {
                        layout: {
                          hierarchical: {
                            direction: "UD",
                            sortMethod: "directed",
                          },
                        },
                        interaction: { dragNodes: false },
                        physics: {
                          enabled: false,
                        },
                        configure: {
                          filter: function (option, path) {
                            if (path.indexOf("hierarchical") !== -1) {
                              return true;
                            }
                            return false;
                          },
                          showButton: false,
                        },
                    };
    
                    // initialize your network!
                    var network = new vis.Network(container, data, options);
                </script>
                </body>
                </html>"""
            file = open("lateral_movement_visual.html", 'w')
            file.write(html_lateral_movement_visual)
            file.close()
            webbrowser.open_new_tab("lateral_movement_visual.html")

        # converting the information from the smb_connection & json_beacon_lateral_movement into a network graph that resembles the injection path #
        def communication_visual(self, smb_connection, json_beacon_lateral_movement):
            string_nodes = ''
            string_edges = ''
            list_hosts = []
            list_connections = []

            # getting all the lateral movement systems (nodes), these include the smb systems
            for information in json_beacon_lateral_movement:
                key = list(information.keys())[0]
                host = key
                colour = 'rgb(235, 107, 52)'
                hostname = information[key]['hostname']
                connection = information[key]['connected']
                if host not in list_hosts:
                    if host == 'C2 server':
                        string_nodes += '                {id: ' + "'" + host + "'" + ', label: ' + "'" + hostname + "'" + ', font: {size: 12, color: "black", face: "arial"}' + ', color: "rgb(235, 107, 52)"' + '},\n'
                    else:
                        # the extra space aligns the lines, check HTML code #
                        string_nodes += '                {id: ' + "'" + host + "'" + ', label: ' + "'" + host.split(', ')[
                            1] + "'" + ', font: {size: 12, color: "black", face: "arial"}' + '},\n'
                        list_hosts.append(host)
                if host + '->' + connection not in list_connections:
                    string_edges += '                {from: ' + "'" + host + "'" + ', to: ' + "'" + connection + "'" + ', color: "' + colour + '"' + ', arrows: "to"' + '},\n'
                    list_connections.append(host + '->' + connection)

            # getting all the connections (edges) in place
            for information in smb_connection:
                if ',' in information and ', ' in smb_connection[information]['child']:
                    if information.split(', ')[1] != smb_connection[information]['child'].split(', ')[1]:
                        key = information
                        parent = key.split(', ')[0] + ', ' + key.split(', ')[1]
                        child = smb_connection[information]['child'].split(', ')[0] + ', ' + smb_connection[information]['child'].split(', ')[1]
                        if parent + '->' + child not in list_connections:
                            string_edges += '                {from: ' + "'" + parent + "'" + ', to: ' + "'" + child + "'" + ', arrows: "to"' + '},\n'
                            list_connections.append(parent + '->' + child)

            html_smb_communication_visual = """
                            <html>
                            <head>
                                <script type="text/javascript" src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>

                                <style type="text/css">
                                    #mynetwork {
                                        width: 1800px;
                                        height: 800px;
                                        border: 1px solid rgb(26, 9, 182);
                                    }
                                </style>
                            </head>
                            <body>
                                <div id="mynetwork"><div class="vis-network" tabindex="0" style="position: relative; overflow: hidden; touch-action: pan-y; 
                                user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 100%;">
                                <canvas width="600" height="540" style="position: relative; touch-action: none; user-select: none; -webkit-user-drag: none; 
                                -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 100%;"></canvas></div></div>

                            <script type="text/javascript">
                                // create an array with nodes
                                var DIR = "images/network-components/";

                                var nodes = new vis.DataSet([
                                    """ + string_nodes + """
                                ]);

                                // create an array with edges
                                var edges = new vis.DataSet([
                                    """ + string_edges + """
                                ]);

                                // create a network
                                var container = document.getElementById('mynetwork');

                                // provide the data in the vis format
                                var data = {
                                    nodes: nodes,
                                    edges: edges
                                };
                                var options = {
                                    nodes: {
                                        shape: 'dot',
                                        size: 10,
                                    },
                                };

                                // initialize your network!
                                var network = new vis.Network(container, data, options);
                            </script>
                            </body>
                            </html>"""

            file = open("html_smb_communication_visual.html", 'w')
            file.write(html_smb_communication_visual)
            file.close()
            webbrowser.open_new_tab("html_smb_communication_visual.html")

# start the program
if __name__ == '__main__':
    CobaltStrikeVisualizer = CobaltStrikeVisualizer()
    CobaltStrikeVisualizer.main()
